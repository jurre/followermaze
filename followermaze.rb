require_relative "lib/client_repository"
require_relative "lib/event"
require_relative "lib/event_router"
require_relative "lib/event_buffer"
require_relative "lib/user_repository"
require_relative "lib/user"
require_relative "lib/client"
require_relative "lib/event_source_server"
require_relative "lib/client_server"

Thread.abort_on_exception = true

event_source_server = EventSourceServer.new(port: 9090)
client_server = ClientServer.new(port: 9099)

[event_source_server, client_server].map do |server|
  Thread.new { server.start }
end.each(&:join)
