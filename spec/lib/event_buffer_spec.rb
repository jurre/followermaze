require "rspec"
require "event_buffer"
require "event"
require "event_router"

describe EventBuffer do
  it "processes events sequentially" do
    allow(EventRouter).to receive(:call).twice
    buffer = EventBuffer.new
    buffer.add(double(id: 1))
    buffer.add(double(id: 2))
    buffer.add(double(id: 4))

    buffer.deliver_buffered_events

    expect(EventRouter).to have_received(:call).twice
  end
end
