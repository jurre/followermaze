require "rspec"
require "user"

describe User do
  it "can add followers" do
    user = User.new(1)
    user.add_follower(2)
    expect(user.followers).to include(2)
  end
end
