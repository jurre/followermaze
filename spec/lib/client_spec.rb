require "rspec"
require "client"

describe Client do
  it "exposes an id" do
    client = Client.new(1, double)
    expect(client.id).to eq(1)
  end

  it "can be notified" do
    connection = double(puts: true)
    client = Client.new(1, connection)
    client.notify("yes")
    expect(connection).to have_received(:puts).with("yes")
  end

  it "can close its connection" do
    connection = double(close: true)
    client = Client.new(1, connection)
    client.close_connection
    expect(connection).to have_received(:close)
  end
end
