require "rspec"
require "event"
require "event_router"
require "user"
require "client_repository"
require "user_repository"

describe EventRouter do
  before(:each) do
    ClientRepository.destroy_all
  end

  it "handles the follow event" do
    mock_client = double(id: 50, notify: true)
    user = UserRepository.add(User.new(50))
    ClientRepository.add(mock_client)
    event = Event.from_payload("666|F|60|50")

    EventRouter.call(event)

    expect(mock_client).to have_received(:notify).with(event.payload)
    expect(user.followers).to include(60)
  end

  it "handles the broadcast event" do
    mock_client1 = double(id: 1, notify: true)
    mock_client2 = double(id: 2, notify: true)
    ClientRepository.add(mock_client1)
    ClientRepository.add(mock_client2)
    event = Event.from_payload("2|B")
    
    EventRouter.call(event)

    expect(mock_client1).to have_received(:notify).with(event.payload)
    expect(mock_client2).to have_received(:notify).with(event.payload)
  end

  it "handles the private message event" do
    mock_client = double(id: 56, notify: true)
    mock_client2 = double(id: 666, notify: true)
    ClientRepository.add(mock_client)
    ClientRepository.add(mock_client2)
    event = Event.from_payload("43|P|32|56")

    EventRouter.call(event)

    expect(mock_client).to have_received(:notify).with(event.payload)
    expect(mock_client2).to_not have_received(:notify)
  end

  it "handles the status update event" do
    mock_client = double(id: 666, notify: true)
    mock_client2 = double(id: 555, notify: true)
    mock_client3 = double(id: 444, notify: true)
    ClientRepository.add(mock_client)
    ClientRepository.add(mock_client2)
    ClientRepository.add(mock_client3)
    user = User.new(32)
    UserRepository.add(user)
    user.add_follower(666)
    user.add_follower(555)
    event = Event.from_payload("634|S|32")

    EventRouter.call(event)

    expect(mock_client).to have_received(:notify).with(event.payload)
    expect(mock_client2).to have_received(:notify).with(event.payload)
    expect(mock_client3).to_not have_received(:notify).with(event.payload)
  end
end
