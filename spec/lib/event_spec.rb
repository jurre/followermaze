require "rspec"
require "event"

describe Event do
  it "can be initialized from a payload" do
    event = Event.from_payload("14|F|489|282")
    expect(event).to be_a Event
  end

  it "has the correct type" do
    event = Event.from_payload("14|F|489|282")
    expect(event.type).to eq(Event::FOLLOW)
  end
end
