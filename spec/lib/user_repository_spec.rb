require "rspec"
require "user_repository"
require "user"

describe UserRepository do
  before(:each) do
    UserRepository.destroy_all
  end

  it "can add and find users" do
    user = double(id: 1)
    UserRepository.add(user)
    expect(UserRepository.find(1)).to eq(user)
  end

  it "can list all the users" do
    user1 = double(id: 1)
    user2 = double(id: 2)
    UserRepository.add(user1)
    UserRepository.add(user2)
    expect(UserRepository.all).to eq({ 1 => user1, 2 => user2 })
  end

  describe "#find_or_add" do
    it "adds users if they don't exist" do
      UserRepository.find_or_add(1)
      expect(UserRepository.find(1)).to_not be_nil
    end

    it "returns the existing user if it exists" do
      user = double(id: 2)
      UserRepository.add(user)
      expect(UserRepository.find_or_add(2)).to eq(user)
    end
  end
end
