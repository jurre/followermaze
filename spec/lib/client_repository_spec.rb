require "rspec"
require "client_repository"

describe ClientRepository do
  it "can add and find clients" do
    client = double(id: 1)
    ClientRepository.add(client)
    expect(ClientRepository.find(1)).to eq(client)
  end

  it "can list all the keys" do
    ClientRepository.add(double(id: 1))
    ClientRepository.add(double(id: 2))
    expect(ClientRepository.all_keys).to eq([1, 2])
  end

  it "can list all the clients" do
    client1 = double(id: 1)
    client2 = double(id: 2)
    ClientRepository.add(client1)
    ClientRepository.add(client2)
    expect(ClientRepository.all).to eq({ 1 => client1, 2 => client2 })
  end

  it "can empty the repository" do
    ClientRepository.add(double(id: 1))
    ClientRepository.add(double(id: 2))
    ClientRepository.destroy_all
    expect(ClientRepository.all).to eq({})
  end
end
