require "rspec"
require "client_repository"
require "event"
require "event_router"
require "event_buffer"
require "user_repository"
require "user"
require "client"
require "event_source_server"
require "client_server"

describe "Running the application" do
  it "successfully processes 10000 events" do
    event_port = 5050
    client_port = 5055
    app = Thread.new do
      event_source_server = EventSourceServer.new(port: event_port)
      client_server = ClientServer.new(port: client_port)

      [event_source_server, client_server].map do |server|
        Thread.new { server.start }
      end.each(&:join)
    end

    output = %x(
      cd #{File.expand_path(File.dirname(__FILE__))}/support
      export totalEvents=10000
      export concurrencyLevel=10

      export eventListenerPort=#{event_port}
      export clientListenerPort=#{client_port}

      ./followermaze.sh
    )

    expect(output).to include("ALL NOTIFICATIONS RECEIVED")
    app.kill
  end
end
