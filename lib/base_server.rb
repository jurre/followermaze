require "socket"

class BaseServer
  def initialize(port:)
    @server = TCPServer.open(port)
    @server.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)
  end
end

