class ClientRepository
  def self.all
    @_clients ||= {}
  end

  def self.find(id)
    all[id]
  end
  
  def self.add(client)
    all[client.id] = client
  end

  def self.all_keys
    all.keys
  end

  def self.destroy_all
    @_clients = {}
  end
end
