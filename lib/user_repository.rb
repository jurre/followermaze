class UserRepository
  def self.all
    @_users ||= {}
  end

  def self.find(id)
    all[id]
  end

  def self.add(user)
    all[user.id] = user
  end

  def self.find_or_add(id)
    find(id) || add(User.new(id))
  end

  def self.destroy_all
    @_users = {}
  end
end
