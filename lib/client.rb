class Client
  attr_reader :id

  def initialize(id, connection)
    @id = id
    @connection = connection
  end

  def notify(message)
    @connection.puts(message)
  end

  def close_connection
    @connection.close
  end
end
