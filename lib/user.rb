require "set"

class User
  attr_reader :id, :followers

  def initialize(id)
    @id = id.to_i
    @followers = Set.new
  end

  def add_follower(follower)
    @followers << follower
  end

  def remove_follower(follower)
    @followers.delete(follower)
  end
end
