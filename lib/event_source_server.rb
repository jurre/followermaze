require_relative "./base_server"

class EventSourceServer < BaseServer
  def initialize(port: 9090)
    super
    @buffer = EventBuffer.new
  end

  def start
    connection = @server.accept

    while payload = connection.gets do
      event = Event.from_payload(payload.strip)
      @buffer.add(event)
      @buffer.deliver_buffered_events
    end

    connection.close
  end
end
