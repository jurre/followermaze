require_relative "./base_server"

class ClientServer < BaseServer
  def start
    while connection = @server.accept do
      user_id = connection.gets.strip.to_i
      ClientRepository.add(Client.new(user_id, connection))
      UserRepository.add(User.new(user_id))
    end

    connection.close
  end
end
