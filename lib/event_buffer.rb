class EventBuffer
  def initialize
    @events = {}
    @next_id = 1
  end

  def add(event)
    @events[event.id] = event
  end

  def deliver_buffered_events
    while @events.has_key? @next_id
      event = @events.delete @next_id
      EventRouter.call(event)
      @next_id += 1
    end
  end
end
