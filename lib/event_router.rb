class EventRouter
  attr_reader :event

  def initialize(event)
    @event = event
  end

  def self.call(event)
    new(event).handle_event
  end

  def handle_event
    STRATEGIES[event.type][event]
  end

  def self.notify_clients(clients, payload)
    Array(clients).each do |client_id|
      client = ClientRepository.find(client_id)
      client.notify(payload) if client
    end
  end

  STRATEGIES = {
    Event::FOLLOW => ->(event) {
      user = UserRepository.find_or_add(event.to)
      user.add_follower(event.from)
      notify_clients(event.to, event.payload)
    },
    Event::BROADCAST => ->(event) {
      notify_clients(ClientRepository.all_keys, event.payload)
    },
    Event::PRIVATE_MESSAGE => ->(event) {
      notify_clients(event.to, event.payload)
    },
    Event::STATUS_UPDATE => ->(event) {
      user = UserRepository.find_or_add(event.from)
      notify_clients(user.followers, event.payload)
    },
    Event::UNFOLLOW => ->(event) {
      user = UserRepository.find(event.to)
      user.remove_follower(event.from) if user
    }
  }
end
