class Event
  TYPES = {
    "F" => FOLLOW = :follow,
    "U" => UNFOLLOW = :unfollow,
    "B" => BROADCAST = :broadcast,
    "P" => PRIVATE_MESSAGE = :private_message,
    "S" => STATUS_UPDATE = :status_update,
  }

  attr_reader :id, :type, :from, :to, :payload

  def initialize(id:, type:, from:, to:, payload:)
    @id = id.to_i
    @type = TYPES.fetch(type)
    @from = from.to_i
    @to = to.to_i
    @payload = payload
  end

  def self.from_payload(payload)
    id, type, from, to = payload.split("|")
    new(id: id, type: type, from: from, to: to, payload: payload)
  end
end
