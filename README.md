FollowerMaze
------------

To run the app:

```bash
ruby followermaze.rb
```

## Requirements
Ruby 2.3.3


## Running tests
```bash
bundle exec rspec
```
